#!/usr/bin/env python2

import executive_engine_api as api
import rospy
'''
This is a simple mission, the drone takes off, follows a path and lands
There is a little detail in the way of activating the behaviors:
1.activateBehavior. Activates the behavior and let other behaviors run.
2.executeBehavior. Activates the behavior and do not let other behaviors run.
'''
def runMission():
  print("Starting mission...")

  print("Taking off...")
  api.executeBehavior('TAKE_OFF')

  print("Waiting...")
  api.executeBehavior('WAIT', duration = 15)	
  #print("ROTATE_WITH_PID_CONTROL...")
  #api.executeBehavior('ROTATE_WITH_PID_CONTROL', relative_angle = 270)	

  #print("FOLLOW_PATH...")
  #api.executeBehavior('FOLLOW_PATH', path = [ [0, 2, 1] , [2, 2, 1] , [2, 0, 1] , [0, 0, 1] ])	

  print('Finish mission...')

