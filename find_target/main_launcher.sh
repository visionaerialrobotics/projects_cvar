#!/bin/bash

NUMID_DRONE=111
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects_cvar/find_target

. ${AEROSTACK_STACK}/setup.sh
 

#---------------------------------------------------------------------------------------------
# INTERNAL PROCESSES
#---------------------------------------------------------------------------------------------
gnome-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# Basic Behaviors                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Basic Behaviors" --command "bash -c \"
roslaunch basic_quadrotor_behaviors basic_quadrotor_behaviors.launch --wait \
    namespace:=drone$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor Motion With PID Control                                                           ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Quadrotor Motion With PID Control" --command "bash -c \"
roslaunch quadrotor_motion_with_pid_control quadrotor_motion_with_pid_control.launch --wait \
    namespace:=drone$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Driver Rotors                                                                               ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Driver Rotors" --command "bash -c \"
roslaunch rotors_interface rotors_interface.launch --wait \
    drone_namespace:=drone$NUMID_DRONE \
    rotors_drone_id:=$DRONE_SWARM_ID \
    rotors_drone_model:=hummingbird;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Midlevel Controller                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Midlevel Controller" --command "bash -c \"
roslaunch droneMidLevelAutopilotROSModule droneMidLevelAutopilotROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Request operator assistance                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Request Operator Assistance" --command "bash -c \"
roslaunch behavior_request_operator_assistance behavior_request_operator_assistance.launch --wait \
    namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Execution Viewer                                                                                ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Execution Viewer" --command "bash -c \"
roslaunch execution_viewer execution_viewer.launch --wait \
  robot_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Robot Localization                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Robot Localization" --command "bash -c \"
roslaunch droneRobotLocalizationROSModule droneRobotLocalizationROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# EKF Localization                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "EKF Localization" --command "bash -c \"
roslaunch ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/launch_files/Ekf.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor controller                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Quadrotor Controller" --command "bash -c \"
roslaunch quadrotor_pid_controller_process quadrotor_pid_controller_process.launch --wait \
    robot_namespace:=drone$NUMID_DRONE \
    robot_config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Executive Coordinator                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Executive Coordinator" --command "bash -c \" sleep 5;
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  behavior_catalog_path:=${AEROSTACK_PROJECT}/configs/mission/behavior_catalog.yaml \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Path tracker                                                                                ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Path tracker" --command "bash -c \"
roslaunch path_tracker_process path_tracker_process.launch --wait \
    robot_namespace:=drone$NUMID_DRONE \
    robot_config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE;
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Process Manager                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Process Manager" --command "bash -c \"
roslaunch process_manager_process process_manager_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Python Interpreter                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Python Interpreter" --command "bash -c \"
roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  mission_configuration_folder:=${AEROSTACK_PROJECT}/configs/mission ;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Manager                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Manager" --command "bash -c \"
roslaunch belief_manager_process belief_manager_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    config_path:=${AEROSTACK_PROJECT}/configs/mission \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Updater                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Updater" --command "bash -c \"
roslaunch belief_updater_process belief_updater_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Self Localization Selector Process                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Self Localization Selector" --command "bash -c \"
roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" &
  #---------------------------------------------------------------------------------------------
# SHELL INTERFACE
#---------------------------------------------------------------------------------------------
gnome-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# alphanumeric_viewer                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "alphanumeric_viewer"  --command "bash -c \"
roslaunch alphanumeric_viewer alphanumeric_viewer.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  &

sleep 5

gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Tree Interpreter                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Behavior Tree Interpreter" --command "bash -c \"
roslaunch behavior_tree_interpreter behavior_tree_interpreter.launch --wait \
  robot_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  mission_configuration_folder:=${AEROSTACK_PROJECT}/configs/mission ;
exec bash\""  &