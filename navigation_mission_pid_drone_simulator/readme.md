#  Navigation mission with Drone simulator 

In order to install and execute this project, perform the following steps.

### Install the project

- Download the installation files:

        $ git clone https://bitbucket.org/visionaerialrobotics/aerostack_installers.git ~/temp

- Run the following installation script to install the project "basic_mission_pid_drone_simulator":

        $ ~/temp/install_project.sh projects_cvar/navigation_mission_pid_drone_simulator

### Execute the project

- Open a new terminal and launch roscore to start the ROS master node:

        $ roscore

- Change directory to this project:

        $ cd $AEROSTACK_STACK/projects_cvar/navigation_mission_pid_drone_simulator

- Execute the script that launches the Aerostack components for this project:
 
        $ ./main_launcher.sh


- Execute the following command to run the mission:

        $ rosservice call /drone107/python_based_mission_interpreter_process/start


- To stop the processes execute the following script:

        $ ./stop.sh

- To close the inactive terminals:

        $ killall bash