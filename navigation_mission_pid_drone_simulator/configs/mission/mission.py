#!/usr/bin/env python2

import executive_engine_api as api
import rospy

def runMission():
  print("Starting mission...")
  print("Taking off...") 
  api.executeBehavior('TAKE_OFF')
  print("Generating path...")
  api.executeBehavior('GENERATE_PATH_2D_WITH_GEOMETRY_MAP', destination = [20, 0, 1])
  success , unification = api.queryBelief('path(?x,?y)')
  if success:
    traject = unification['y']
    traject = eval(traject)
    traject = list(traject)
    result = [] 
    i=0
    pointTraj=traject[0]
    while i < len(traject)-1: 
     pointTraj= traject[i]
     result.append(list(traject[i])) 
     i+=1
    result.append(list(traject[len(traject)-1])) 
    print "Path: "
    print result
    print "Following path..."
    api.executeBehavior('FOLLOW_PATH', path = result)  
    print('Finishing mission...')
  else:
    print('No path was found')
