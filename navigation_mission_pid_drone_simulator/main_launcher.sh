#!/bin/bash

NUMID_DRONE=107
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects_cvar/navigation_mission_pid_drone_simulator

. ${AEROSTACK_STACK}/setup.sh

#---------------------------------------------------------------------------------------------
# INTERNAL PROCESSES
#---------------------------------------------------------------------------------------------
xfce4-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# Basic Behaviors                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Basic Behaviors" --command "bash -c \"
roslaunch basic_quadrotor_behaviors basic_quadrotor_behaviors.launch --wait \
    namespace:=drone$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Navigation with geometry map                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Navigation with geometry map" --command "bash -c \"
roslaunch navigation_with_geometry_map navigation_with_geometry_map.launch --wait \
    namespace:=drone$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor Motion With PID Control                                                           ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Quadrotor Motion With PID Control" --command "bash -c \"
roslaunch quadrotor_motion_with_pid_control quadrotor_motion_with_pid_control.launch --wait \
    namespace:=drone$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor simulator                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Simulator" --command "bash -c \"
roslaunch droneSimulatorROSModule droneSimulatorROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Trajectory planner                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Trajectory Planner" --command "bash -c \"
roslaunch droneTrajectoryPlannerROSModule droneTrajectoryPlanner2dROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT} \
    drone_pose_topic_name:=estimated_pose;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Self localizer (State estimator)                                                            ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "State Estimator" --command "bash -c \"
roslaunch droneEKFStateEstimatorROSModule droneEKFStateEstimatorROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor controller                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Quadrotor Controller" --command "bash -c \"
roslaunch quadrotor_pid_controller_process quadrotor_pid_controller_process.launch --wait \
    robot_namespace:=drone$NUMID_DRONE \
    robot_config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Path tracker                                                                                ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Path tracker" --command "bash -c \"
roslaunch path_tracker_process path_tracker_process.launch --wait \
    robot_namespace:=drone$NUMID_DRONE \
    robot_config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE;
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Process monitor                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Process Monitor" --command "bash -c \"
roslaunch process_monitor_process process_monitor.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Python Interpreter                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Python Interpreter" --command "bash -c \"
roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  mission_configuration_folder:=${AEROSTACK_PROJECT}/configs/mission ;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Manager                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Manager" --command "bash -c \"
roslaunch belief_manager_process belief_manager_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    config_path:=${AEROSTACK_PROJECT}/configs/mission \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Updater                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Updater" --command "bash -c \"
roslaunch belief_updater_process belief_updater_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Self Localization Selector Process                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Self Localization Selector" --command "bash -c \"
roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Obstacle distance calculator                                                                ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Obstacle Distance Calculator" --command "bash -c \"
roslaunch droneObstacleDistanceCalculatorROSModule droneObstacleDistanceCalculationROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT} \
    drone_pose_topic_name:=estimated_pose;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Obstacle Processor                                                                          ` \
`# Identifies obstacles according to the Visual Markers                                        ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Obstacle Processor" --command "bash -c \"
roslaunch obstacle_detector_visual_markers_2d_process obstacle_detector_visual_markers_2d_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    obstacles_topic_name:=obstacles \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""   \
`#---------------------------------------------------------------------------------------------` \
`# Executive Coordinator                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Executive Coordinator" --command "bash -c \" sleep 5;
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  behavior_catalog_path:=${AEROSTACK_PROJECT}/configs/mission/behavior_catalog.yaml \
  my_stack_directory:=${AEROSTACK_PROJECT}/projects_cvar/basic_mission_pid_drone_simulator;
exec bash\"" &

#---------------------------------------------------------------------------------------------
# USER INTERFACE PROCESSES
#---------------------------------------------------------------------------------------------
xfce4-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Environment Viewer                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Environment Viewer"  --command "bash -c \"
roslaunch environment_viewer environment_viewer.launch  --wait \
    robot_namespace:=drone$NUMID_DRONE \
    robot_config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Execution Viewer                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Execution Viewer" --command "bash -c \"
roslaunch execution_viewer execution_viewer.launch --wait \
  robot_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Alphanumeric Viewer                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Alphanumeric Viewer"  --command "bash -c \"
roslaunch alphanumeric_viewer alphanumeric_viewer.launch  --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  &
